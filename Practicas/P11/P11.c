//Author: Mariano Hurtado de Mendoza Carranza
//Date: September 13 2019
//ID: A00820039
//Description: Program that reads binary and converts to integer, hex to integer
//ASCII OCT to integer, and divides and multiplies by 2 using bit operations

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>   
#include <regex.h>

extern int errno;

//Function Calulating the power of integers
int intPow(int iNum, int iPow){
    int iProd = 1;
    for(int i = 0; i < iPow; i++){
        iProd *= iNum;
    }

    return iProd;
}

//Function that converts ascii binary into decimals
int asciiBinaryToInt(char *cArrS){
    bool bValid = true;
    int iSum = 0;
    size_t stBit = strlen(cArrS) - 1;

    for(char *cPtrCheck = cArrS; *cPtrCheck != 0 && bValid; cPtrCheck++){
        if(*cPtrCheck == '0' || *cPtrCheck == '1'){
            iSum += intPow(2, stBit) * (*cPtrCheck - 48);
        }
        else{
            bValid = false;
        }
        stBit--;
    }

    if(bValid){
        return iSum;
    }

    return -1;
}

//Function that converts hex to decimal
int asciiHexToInt(char *cArrS){
    bool bValid = true;
    int iSum = 0;
    size_t stBit = strlen(cArrS) - 1;

    for(char *cPtrCheck = cArrS; *cPtrCheck != 0 && bValid; cPtrCheck++){
        if(*cPtrCheck >= 'A' && *cPtrCheck <= 'F'){
            iSum += intPow(16, stBit) * (*cPtrCheck - 65 + 10);
        }
        else if(*cPtrCheck >= 'a' && *cPtrCheck <= 'f'){
            iSum += intPow(16, stBit) * (*cPtrCheck - 97 + 10);
        }
        else if(*cPtrCheck >= '0' && *cPtrCheck <= '9'){
            iSum += intPow(16, stBit) * (*cPtrCheck - 48);
        }
        else{
            bValid = false;
        }
        stBit--;
    }

    if(bValid){
        return iSum;
    }

    return -1;
}

//Function that calculates ascii oct to integer
int asciiOctToInt(char *cArrS){
    char *cPtrCheck;
    int iSum = 0;
    regex_t rExpresion;
    size_t stBit = strlen(cArrS)-1;
    regcomp(&rExpresion,"[0-7]+",REG_EXTENDED|REG_NOSUB);
    if(!regexec(&rExpresion,cArrS,0,NULL,0)){
        regfree(&rExpresion);
        for(cPtrCheck = cArrS; *cPtrCheck != 0; cPtrCheck++){
            iSum += intPow(8,stBit) * (*cPtrCheck - 48);
            stBit--;
        }
        return iSum;
    }
    return -1;
}//Function that divides by two using bit operator
int divideByTwo(int *i){
    return *i >> 1;
}

//Function that multiplies by two using bit operator
int multiplyByTwo(int *i){
    return *i << 1;
}

//Main Function
int main()
{
    //Variables for the options.
    int iOpcion, iNum;
    char cArrS[32];
    //Loop displaying menu and solutions
    do{
        printf("------MENU-------\n");
        printf("1. Binary to Decimal\n");
        printf("2. Hex to Decimal\n");
        printf("3. ASCII OCT to INT\n");
        printf("4. Multiply By two\n");
        printf("5. Divide by two\n");
        printf("0. Exit\n");
        printf("Select Option number: ");
        scanf("%d", &iOpcion);

        switch(iOpcion){
            case 1:
                printf("Input ascii binary: ");
                scanf("%s", cArrS);
                printf("%d\n", asciiBinaryToInt(cArrS));
                break;
            case 2:
                printf("Input ascii hex: ");
                scanf("%s", cArrS);
                printf("%d\n", asciiHexToInt(cArrS));
                break;
            case 3:
                printf("Input ascii Oct: ");
                scanf("%s", cArrS);
                printf("%d\n", asciiOctToInt(cArrS));
                break;
            case 4:
                printf("Input Number: ");
                scanf("%d", &iNum);
                printf("%d\n", multiplyByTwo(&iNum));
                break;
            case 5:
                printf("Input Number: ");
                scanf("%d", &iNum);
                printf("%d\n", divideByTwo(&iNum));
                break;
                
        }

    }while(iOpcion != 0);
    return 0;
}