//Mariano Hurtado de Mendoza Carranza A00820039
//Date: September 10 2019
//Description: create a function that can allocate a large string in memory

//Include the necessary libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Declaration of the function
char *extremelyLargeString(){
    //Counter and Max that control the size of a given string
    int iCounter = 0;
    int iMax = 5;

    //Large string to be allocated
    char *sLargeString;
    char cAux; //Char that will be read through out the function;
    printf("Write a extremely large string: ");

    //Allocating memory to sLargeString
    sLargeString = (char*) malloc(iMax * sizeof(char));

    //Loop used to read the string
    while((cAux = getchar()) != '\n'){
        sLargeString[iCounter]= cAux;
        iCounter++;
        if(iCounter == iMax){
            iMax *= 5;
            //reallocation of memory when the string is large enough
            sLargeString = (char*) realloc(sLargeString,sizeof(char) * iMax);
        }
    }
    //Concatinating a null character inside of the large string so no trash is stored
    sLargeString = strcat(sLargeString, "\0");

    return sLargeString;
}

//Main function
int main(void){
    char *sLargeString;

    //Implementation of extremelyLargeString() function
    sLargeString = extremelyLargeString();
    //Printing the string

    printf("%s\n", sLargeString);
    
    free(sLargeString); //Freeing memory

    return 0;
}