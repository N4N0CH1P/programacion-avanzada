//Author: Mariano Hurtado de Mendoza Carranza
//ID: A00820039
//Date: June 19 2019

//Given a data type, print the value and the size in bytes of the data type

//Including std output and input
//Including float library for the needed macros
#include <stdio.h>
#include <float.h>

//Main Function
int main(void){
    //Declaration of the needed variables
    int iNum;
    char cCharacter;
    double dDecimal;

    //Asking for user input
    printf("input a integer, character, and a double: ");
    scanf("%d %c %lf", &iNum, &cCharacter, &dDecimal);

    //Displaying the information that the user wants to see
    printf("Your integer %d storage size is %ld bytes\n", iNum, sizeof(iNum));
    printf("Your character %c storage size is %ld bytes.",cCharacter, sizeof(cCharacter)); 
    printf("And I can read it as %c or as %d. \n", cCharacter, cCharacter);
    printf("Your double %lf storage size is %ld bytes, I can read any number from ",dDecimal,sizeof(dDecimal));
    printf("%lf to %lf in this data type.\n", __DBL_MIN__, __DBL_MAX__);

}
