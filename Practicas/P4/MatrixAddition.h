#include <stdio.h>

//Function for the matrix addition
void matrixAddition(int iMatA[2][2], int iMatB[2][2]){
    int iMatSol[2][2];

    //Loop to add matrix
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 2; j++){
            iMatSol[i][j] = iMatA[i][j] + iMatB[i][j];
        }
    }

    //Loop that displays the matrix
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 2; j++){
            printf("%d ",iMatSol[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}