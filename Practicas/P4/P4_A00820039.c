//Author: Mariano Hurtado de Mendoza Carranza
//ID: A00820039
//Date: August 20 2019
//This program calculates the matrix multiplication, addition and subtraction of a given
//matrix

//Including necessary headers
#include <stdio.h>

//Headers that have the functions for the operations
#include "MatrixAddition.h"
#include "MatrixSubtraction.h"
#include "MatrixMultiplication.h"

//Main function
int main(char *args[]){
    //Matrixes to be processed
    int iMatA[2][2], iMatB[2][2];

    //Scanning the values needed
    printf("Give me 4 integers for matrix A: ");
    for(int i = 0; i < 2; i++)
        for(int j = 0; j < 2; j++)
            scanf("%d", &iMatA[i][j]);
    
    printf("Give me 4 integers for matrix B: ");
    for(int i = 0; i < 2; i++)
        for(int j = 0; j < 2; j++)
            scanf("%d", &iMatB[i][j]);
    
    //Printing the result
    printf("Addition:\n");
    matrixAddition(iMatA,iMatB);
    printf("Subtraction:\n");
    matrixSubtraction(iMatA,iMatB);
    printf("Multiplication:\n");
    matrixMultiplication(iMatA,iMatB);

}