#include <stdio.h>
void matrixMultiplication(int iMatA[2][2], int iMatB[2][2]){
    //Declare a matrix initialized with 0
    int iMatSol[2][2] = {{0,0},{0,0}};

    //Loop for multiplying the matrix
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 2; j++){
            for(int k = 0; k < 2; k++)
             iMatSol[i][j] += iMatA[i][k] * iMatB[k][j];
        }
    }

    //Loop that prints the array
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 2; j++){
            printf("%d ",iMatSol[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}