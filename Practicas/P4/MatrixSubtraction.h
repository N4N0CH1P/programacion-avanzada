#include <stdio.h>
//Function that calculate matrix subtraction
void matrixSubtraction(int iMatA[2][2], int iMatB[2][2]){
    int iMatSol[2][2];
    //Loop needed to subtract the matrixes
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 2; j++){
            iMatSol[i][j] = iMatA[i][j] - iMatB[i][j];
        }
    }
    //Loop needed to print the matrix solution
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 2; j++){
            printf("%d ",iMatSol[i][j]);
        }

        printf("\n");
    }
    printf("\n");
}