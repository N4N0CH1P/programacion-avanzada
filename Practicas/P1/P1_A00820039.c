//Author: Mariano Hurtado de Mendoza Carranza 
//ID: A00820039
//Date: June 19 2019
 
//This program has 2 sections: Calculate the fibonacci sequence given the number
//of digits and to check if a given number is prime or not

//Include input and output library
#include <stdio.h>

//Functions

//Fibonacci sequence function
void fibonacci(int iMax){
    int iNum0 = 0, iNum1 = 1, iAux;

    //Loop used to print and calculate the next sequence
    for(int i = 0; i < iMax; i++){
        printf("%d ", iNum0);
        iAux = iNum1;
        iNum1 = iNum0 + iAux;
        iNum0 = iAux;
    }
    printf("\n");
    
}

//Function that checks prime numbers
int isPrime(int iNum)
{
    int iPrime = 1;

    //Loop used to check if a number is prime
    //Checks from 2 to the number to check
    for(int i = 2; iPrime && i < iNum; i++){
        if(iNum % i == 0)
            iPrime = 0;
    }

    return iPrime;
}

//Main Function
int main(void){
    int iOption, iNumber;

    //Loop used to display a menu with options
    do{
        printf("Menu\n");
        printf("1. Fibonacci\n");
        printf("2. Prime Verification\n");
        printf("0. Exit\n");
        printf("Select option (1/2/0): ");
        scanf("%d", &iOption);

        switch (iOption)
        {
        //Run Fibonacci function
        case 1:
            printf("Enter an integer: ");
            scanf("%d", &iNumber);
            fibonacci(iNumber);
            break;
        
        //Run isPrime function
        case 2:
            printf("Enter an integer: ");
            scanf("%d", &iNumber);

            if(isPrime(iNumber))
                printf("%d is prime\n", iNumber);
            else
                printf("%d is not prime\n", iNumber);

            break;
        
        //Exit Program
        case 0: printf("Good-Bye!\n");
        default: printf("Invalid Option\n");
            break;
        }
    }
    while(iOption != 0);
}
