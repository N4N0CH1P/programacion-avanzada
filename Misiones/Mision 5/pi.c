#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef long double big_d;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
big_d dSum = 0.0;
big_d dX = -1.0;

void *calcularPi(){
    pthread_mutex_lock(&mutex);
    double deltaX = 0.001;
    dSum += sqrt(1-pow(dX,2)) * deltaX;
    dX += deltaX;
    pthread_mutex_unlock(&mutex);
}

int main(){
    pthread_t threads[10];
    int iRet[10];
    while(dX < 0){
        printf("%Lf\n", dX);
        for(int i = 0; i < 10; i++){
            iRet[i] = pthread_create(&threads[i],NULL,&calcularPi,NULL);
        }

        for(int i = 0; i < 10; i++){
            pthread_join(threads[i],NULL);
         }
    }
    //dSum *= 2;
    printf("Pi= %.12Lg", dSum);

}