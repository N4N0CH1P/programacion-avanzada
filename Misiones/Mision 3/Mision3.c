//Mariano Hurtado de Mendoza Carranza
//A00820039
//Fecha: 22 de septiembre de 2019
//Descripcion: Este programa lee hashtags de una pagina de twitter y los desplieg en pantalla

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

//Se hace una estructura que va a almacenar los hashtags
typedef struct hash{
    char *data;
    int cantidad;
} hashtag;

//Una funcion que utiliza el string sAux1 para poder guardar el hashtag de una linea
void createHashtag(char *sAux1,char *sText){
    char sAux2[1000] = "#";
    char *ptrSearch;
    int iDone = 0;
    for(ptrSearch = sText+1; *ptrSearch != 0 && !iDone; ptrSearch++){
        if(*ptrSearch >= 'a' && *ptrSearch <= 'z' ||
           *ptrSearch >= '0' && *ptrSearch <= '9'){
            int len = strlen(sAux2);
            sAux2[len] = *ptrSearch;
            sAux2[len+1] = 0;
        }
        else{
            iDone = 1;
        }
    }
    strcpy(sAux1,sAux2);
}

//esta funcion ayuda a cambiar todas las letras a minusculas
void toLower(char *sText){
    for (char *ptrSearch = sText; *ptrSearch != 0; ptrSearch++){
        *ptrSearch = tolower(*ptrSearch);
    }
}

//Funcion main
int main(void){
    //Apuntador al archivo
    FILE *fp;
    char sText[1000]; //Aqui se guardan los textos analizados
    //se almacenan los hashtags en un arreglo dinamico
    hashtag *hashtags = (hashtag*) malloc(sizeof(hashtag) * 40);

    int iMaxCant = -1;

    //aqui se almacenaran los hashtags encontrados
    char sAux[1000];
    int iMatch = 0;
    for(int i = 0; i < 40; i++){
        hashtags[i].data = "EMPTY";
        hashtags[i].cantidad = 0;
    }

    //se abre el archivo
    fp = fopen("marco.html","r");

    //se verifica que el archivo se haya abierto
    if(fp == NULL){
        printf("No se encontro el archivo.\n");
        return 1;
    }

    //se lee el archivo
    while (fgets(sText,1000,fp) != NULL){
        iMatch = 0;
        toLower(sText);

        //Se verifica si es hashtag
        if(strchr(sText,'#') != NULL){
            createHashtag(sAux,strchr(sText,'#'));
            //Se almacena en la estructura de hashtags
            for(int i = 0; i <= iMaxCant && !iMatch; i+=1){
                if(strcmp(hashtags[i].data,sAux) == 0){
                    hashtags[i].cantidad++;
                    iMatch = 1;
                    printf("%s %d\n", hashtags[i].data, i);
                }
            }
            if(!iMatch){
            iMaxCant++;
            hashtags[iMaxCant].data = sAux;
            hashtags[iMaxCant].cantidad++;
            }
        }
    }

    for(int i = 0; i <= iMaxCant; i++){
        printf("Hashtag: %s\tCantidad: %d\n", hashtags[i].data, hashtags[i].cantidad);
    }
    free(hashtags);
    fclose(fp);
}