#include <gtk/gtk.h>

//Se cre un widget para el txt field
GtkWidget *txt;
int count = 0;

//Funcion que termina el programa
void end_program(GtkWidget *wid, gpointer ptr){
    gtk_main_quit();
}

//Funcion par ingresar el texto dentro del label
void copy_text(GtkWidget *wid, gpointer ptr){
    const char *input = gtk_entry_get_text(GTK_ENTRY(txt));
    gtk_label_set_text (GTK_LABEL(ptr), input);
}

//Funcion que sirve para incrementar un contador
void count_button(GtkWidget *wid, gpointer ptr){
    char buffer[30];
    count++;
    sprintf(buffer, "Button Pressed %d times", count);
    gtk_label_set_text(GTK_LABEL(ptr), buffer);
}


int main(int argc, char *argv[]){
    //Se inicializan los objetos
    gtk_init (&argc, &argv);

    //Se crean nuevos objetos o widgets gtk
    GtkWidget *win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    GtkWidget *btn = gtk_button_new_with_label("Edit");
    GtkWidget *lbl = gtk_label_new("My Label");
    GtkWidget *btn2 = gtk_button_new_with_label("Count");
    GtkWidget *tbl = gtk_table_new(2,2,5);
    txt = gtk_entry_new();

    //Se connectan los "outlets"
    g_signal_connect(btn, "clicked", G_CALLBACK(copy_text), lbl);
    g_signal_connect(win, "delete_event", G_CALLBACK(end_program), NULL);
    g_signal_connect(btn2, "clicked", G_CALLBACK(count_button), lbl);

    //Se posicionan los elementos en la interfaz grafica
    gtk_table_attach_defaults(GTK_TABLE(tbl), lbl, 0, 1, 0, 1);
    gtk_table_attach_defaults(GTK_TABLE(tbl), btn2, 1, 2, 0, 1);
    gtk_table_attach_defaults(GTK_TABLE(tbl), txt, 0, 1, 1, 2);
    gtk_table_attach_defaults(GTK_TABLE(tbl), btn,1,2,1,2);

    //Se agrega la tabla a la ventana
    gtk_container_add(GTK_CONTAINER(win), tbl);

    //Se muestran todos los widgets de la ventana
    gtk_widget_show_all(win);

    //Se ejecuta la interfaz
    gtk_main ();
    return 0;
}