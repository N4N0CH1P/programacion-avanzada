#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

void *executeThread(){
    while(1)
        printf("Thread executed\n");
}
int main(){
    pthread_t thread;

    int iCheck = pthread_create(&thread,NULL,executeThread,NULL);

    if(iCheck){
        printf("Thread could not be created\n");
    }

    while(1){
        printf("Main function\n");
    }
}