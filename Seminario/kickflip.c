#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *kickflip(void *ptr);
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
int  counter = 0;

main()
{
   int rc1, rc2;
   pthread_t thread1, thread2;
   char * pie2 = "Patear pie";
   char * pie1 = "Hacer pop";
   /* Create independent threads each of which will execute functionC */

   if( (rc1=pthread_create( &thread1, NULL, &kickflip, (void *) pie1)) )
   {
      printf("Thread creation failed: %d\n", rc1);
   }

   if( (rc2=pthread_create( &thread2, NULL, &kickflip, (void *) pie2)) )
   {
      printf("Thread creation failed: %d\n", rc2);
   }

   /* Wait till threads are complete before main continues. Unless we  */
   /* wait we run the risk of executing an exit which will terminate   */
   /* the process and all threads before the threads have completed.   */

   pthread_join( thread1, NULL);
   pthread_join( thread2, NULL); 

   exit(0);
}

void *kickflip(void *ptr)
{
   pthread_mutex_lock( &mutex1 );
   char *pie = (char*) ptr;
   printf("Acción: %s\n",pie);
   pthread_mutex_unlock( &mutex1 );
}